const pino = require('pino')

const DEFAULT_LOG_LEVEL = 'info'

module.exports = {
  improve: 'apostrophe-utils',

  beforeConstruct: (self, options) => {
    // Construct pino logger instance according to module options
    const moduleOptionsPino = options || {}
    const pinoStream = moduleOptionsPino.stream
    const pinoOptions = {
      name: options.apos.shortName,
      level: DEFAULT_LOG_LEVEL,
      ...moduleOptionsPino
    }

    const pinoLogger = pino(pinoOptions, pinoStream)

    // Bind pino methods to retain the internal `this` Pino needs to work
    const proxiedPinoLogger = new Proxy(pinoLogger, {
      get (target, key) {
        // Proxy handler for logging methods that should keep the logger context
        return typeof target[key] === 'function'
          ? target[key].bind(target)
          : target[key]
      }
    })

    // Attach a logger instance to the apostrophe application
    options.logger = (apos) => proxiedPinoLogger
  }
}
