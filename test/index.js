const assert = require('assert')

describe('@flaivour/logger-pino', function () {
  // Apostrophe take some time to start up...
  this.timeout(20000)

  let apos

  // Stop Apostrophe running instances once tests are donce
  after(function (done) {
    require('apostrophe/test-lib/util').destroy(apos, done)
  })

  /**
   * EXISTENCE
   */

  it('should overwrite options of apostrophe-utils', function (done) {
    apos = require('apostrophe')({
      testModule: true,
      migrate: false,
      modules: {
        'apostrophe-db': {
          uri: this.mongoUri
        },
        '@flaivour/logger-pino': {}
      },

      afterInit: function (callback) {
        assert(apos.modules['apostrophe-utils'])
        return callback(null)
      },
      afterListen: function (err) {
        assert(!err)
        done()
      }
    })
  })

  it('should set logger into apostrophe-utils options', function () {
    assert(apos.modules['apostrophe-utils'].options.logger)
  })
})
