const { MongoMemoryServer } = require('mongodb-memory-server')
const chalk = require('chalk')
const success = chalk.bgGreen

// Start up in memory Mongo database for Apostrophe
before(async function () {
  console.log(success('start database'))
  this.mongoServer = new MongoMemoryServer()
  this.mongoUri = await this.mongoServer.getConnectionString()
})

// Stop running database once tests are donce
after(function () {
  console.log(success('stop database'))
  this.mongoServer.stop()
})
