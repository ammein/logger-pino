# Changelog

CLI tool to create changelogs and releases

## [1.0.4] - 2019-04-24
### Added
- Move `pino-pretty` in module dependencies [#6]

## [1.0.3] - 2019-04-19
### Added
- Add `.npmignore` file to avoid publishing non-relevant files for module behavior [#5]

## [1.0.2] - 2019-04-18
### Added
- Use definitive `module-tempalete.yml` externalized in CI dedicated repository [#4]

## [1.0.1] - 2019-04-17
### Added
- Add dedicated `gitlab-ci.yml` file for continuous integration while included one is under development [#3]

## 1.0.0 - 2019-04-17
### Added
- Initial implementation improving apostrophe-utils with a custom logger into options. It allows to use pino for Apostrophe logging methods provided through `self.apos.utils` [#1]
- Improve documentation about module usage into README.md [#2]

[#1]: https://gitlab.com/flaivour/useful-tools/releaser/issues/1
[#2]: https://gitlab.com/flaivour/useful-tools/releaser/issues/2
[#3]: https://gitlab.com/flaivour/useful-tools/releaser/issues/3
[#4]: https://gitlab.com/flaivour/useful-tools/releaser/issues/4
[#5]: https://gitlab.com/flaivour/useful-tools/releaser/issues/5
[#6]: https://gitlab.com/flaivour/useful-tools/releaser/issues/6

[1.0.1]: https://gitlab.com/flaivour/apostrophe-modules/logger-pino/compare/v1.0.0...v1.0.1
[1.0.2]: https://gitlab.com/flaivour/apostrophe-modules/logger-pino/compare/v1.0.1...v1.0.2
[1.0.3]: https://gitlab.com/flaivour/apostrophe-modules/logger-pino/compare/v1.0.2...v1.0.3
[1.0.4]: https://gitlab.com/flaivour/apostrophe-modules/logger-pino/compare/v1.0.3...v1.0.4
