# Contributing to Flaivour

First off, thanks for taking the time to contribute! 🎉

The following is a set of guidelines for contributing to Flaivour and its packages, which are hosted in the Flaivour Group on Gitlab.

# Code of Conduct

In the interest of fostering an open and welcoming environment, we as contributors and maintainers pledge to making participation in our project and our community a harassment-free experience for everyone, regardless of age, body size, disability, ethnicity, sex characteristics, gender identity and expression, level of experience, education, socio-economic status, nationality, personal appearance, race, religion, or sexual identity and orientation. All contributors are expected to abide by the [Code of Conduct](CODE_OF_CONDUCT.md).

# Reporting a Bug

If you come across a bug, please submit an issue in the Gitlab repository. That’s the primary source for bug documentation, so first be sure to search the existing issues and make sure it hasn’t been logged. If it hasn’t, please include the following details as they apply:

- Steps to reproduce the bug
- The expected behavior and actual behavior
- Screenshots or animated .GIFs to help illustrate the behavior
- Code samples

Finally, remember to attach the “Bug” label to the issue, to help use stay organized.

# Fixing Bugs or Submitting Enhancements

If you’ve perused our open issues labeled `bug` and decide to work to resolve one, or you’ve got a new feature that you’d like to commit to the core project, please keep these things in mind:

1. Make sure your pull request includes tests. You may take a look at the `test` folder of the module for more examples.

2. Make sure your pull request changes adheres to our coding standards. The module already has a `.eslintrc` file, just run the command `npm run lint` to verify the code style. Your editor may automatically point out eslint-detected concerns as well.

3. Enhancements should include documentation and include implementation details were applicable.
