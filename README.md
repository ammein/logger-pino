# logger-pino
> ApostropheCMS 2 module which makes it use pino logger

Pino is a very low overhead Node.js logger, inspired by Bunyan. This module make it usable as default logger for ApostropheCMS.

## Installation

Install module dependency
```bash
npm i --save @flaivour/logger-pino
```

Declare it in apostrophe modules

```js
// Modules declaration into `app.js`
modules: {
  '@flaivour/logger-pino': {}
}
```

That's it, module is ready 🎉

## Configuration

By default, all module options declared will be passed to the pino instance created for Apostrophe logs. Few of them are really interesting:

- `level` to define the minimum level from which the logs will actually be displayed in the terminal
- `prettyPrint` format and colors log outputs, more information at https://github.com/pinojs/pino-pretty

**Configuration example:**
```js
// Modules declaration into `app.js`
modules: {
  '@flaivour/logger-pino': {
    level: 'warn',
    prettyPrint: { colorize: true }
  }
}
```

## Usage

This module improve apostrophe-utils logging methods, which means it is totally transparent. Simply use `self.apos.utils.log()` to log message from anywhere.

## License

This is released under the MIT License.
